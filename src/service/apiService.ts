import axios, { AxiosRequestConfig } from "axios";

const instance = axios.create({
  baseURL: '', //api endPoint
  params: {}, 
});

instance.interceptors.request.use(
  function (config) {
    config.headers.common["Content-Type"] = "application/x-www-form-urlencoded";
    return config;
  },
  function (error) {
    console.log("Request Error =>", error);
    alert(error);
    return Promise.reject(error);
  }
);

instance.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response) {
      // eslint-disable-next-line no-console
      console.log("Response Error =>", error.response);
      alert(error.response.data.message ?? error.response.data.statusText);
    } else {
      console.log("Error =>", error);
      alert(error);
    }

    return Promise.reject(error);
  }
);

export async function getData(endPoint) {  
  return await instance.get(endPoint);
}

export async function postData(endPoint, data) {
  return await instance.post(endPoint, data);
}
