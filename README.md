# Vue Skill Test

In this project we are using vue 2 and vuetify. For http calls we are using axois (https://axios-http.com/docs/intro).

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```
